const express = require('express');
const app = express();
var shell = require('shelljs');
const port = 9000;
app.post('/pull', (req, res) => pullRepo(res));
function pullRepo(res) {
    shell.exec('sudo bash ./pull.sh');
    res.writeHead(200);
    res.end();
}
app.listen(port,[], () => console.log(`Example app listening on port ${port}!`))